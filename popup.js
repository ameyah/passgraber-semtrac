document.addEventListener('DOMContentLoaded', function() {

    var isCredentialCaptured = false;
    var sematicGenURL = "https://localhost";
    var activePasswordBox;
    var activeUsernameBox;
    var activePassword = "";
    var activeUsername = "";
    var credentialsSent = false;


    /* Check if login attempt was made */
    chrome.storage.local.get("login_attempt", function(data){
        if(data.login_attempt != undefined) {
            if(data.login_attempt == 1) {
                //Check page for password boxes
                var passwordBoxes = getPasswordBoxes();
                if(passwordBoxes.length == 0) {
                    //inform semantic generator that authentication was successful
                    var URL = sematicGenURL + "/auth?success=1"
                } else {
                    //inform semantic generator that authentication was unsuccessful (mostly)
                    var URL = sematicGenURL + "/auth?success=0"
                }
                var xmlRequest = new XMLHttpRequest();
                if(window.XMLHttpRequest) {
                    xmlRequest.open("GET", URL, false);
                    try {
                        xmlRequest.send();
                    } catch (e) {
                        alert(e);
                    }
                }
                chrome.storage.local.set({"login_attempt": 0}, function(){
                    console.log("Authentication information sent");
                });
            }
        }
    });

    /* Function to select all the password text boxes on the webpage */
    var getPasswordBoxes = function() {
        var inputTags = document.getElementsByTagName("input");
        var passwordTags = [];
        for (var i = 0; i < inputTags.length; i++) {
            if (inputTags[i].type.toLowerCase() === "password") {
                passwordTags.push(inputTags[i]);
            }
        }
        return passwordTags;
    };

    var getTextBoxes = function() {
        var inputTags = document.getElementsByTagName("input");
        var textTags = [];
        for (var i = 0; i < inputTags.length; i++) {
            if (inputTags[i].type.toLowerCase() == "text" || inputTags[i].type.toLowerCase() == "email") {
                textTags.push(inputTags[i]);
            }
        }
        return textTags;
    };

    var extractDomain = function(url) {
        var domain;
        //find & remove protocol (http, ftp, etc.) and get domain
        if (url.indexOf("://") > -1) {
            domain = url.split('/')[2];
        }
        else {
            domain = url.split('/')[0];
        }

        //find & remove port number
        domain = domain.split(':')[0];

        return domain;
    };

    var getUsername = function(passwordBox) {
        //First try to locate a username box with the help of passwordBox element
        var tempElement = passwordBox;
        while(true) {
            tempElement = tempElement.previousElementSibling;
            if(tempElement != null) {
                if (tempElement.tagName.toLowerCase() == "input" &&
                    (tempElement.type.toLowerCase() == "text" || tempElement.type.toLowerCase() == "email")) {
                    if (tempElement.value != null && tempElement.value != "") {
                        return tempElement.value;
                    }
                }
            } else {
                break;
            }
        }
        //code reached here because passwordBox didn't find us the username box
        var textBoxes = getTextBoxes();
        for(var i = 0; i < textBoxes.length; i++) {
            if(textBoxes[i].value != null && textBoxes[i].value != "") {
                return textBoxes[i].value;
            }
        }
        //We didn't find a username box with some value in it. So, just return empty string
        return "";
    };

    /* Function to grab password values and send them to ISI server */

    var process = function() {
        if(window.XMLHttpRequest) {
            //get root domain name
            var domain = extractDomain(window.location.href);
            /*if(activeUsername == "") {
                //get username box
                activeUsername = getUsername(activePasswordBox);
            }*/
            if(activeUsername == "") {
                //get from chrome storage
                chrome.storage.local.get("current_username", function(data){
                    if(data.current_username != undefined) {
                        activeUsername = data.current_username;
                    } else {
                        activeUsername = "";
                    }
                });
            }
            if(activePassword == "") {
                //get fromm chrome storage
                chrome.storage.local.get("current_password", function(data){
                    if(data.current_password != undefined) {
                        activePassword = data.current_password;
                    } else {
                        activePassword = "";
                    }
                });
            }
            var passwordStrengthObj = zxcvbn(activePassword);
            try {
                var passwordWarning = passwordStrengthObj.feedback.warning;
            } catch (e) {
                var passwordWarning = "";
            }
            var URL = sematicGenURL +
                "/transform?pass=" + encodeURIComponent(activePassword) +
                "&user=" + encodeURIComponent(activeUsername) +
                "&url=" + domain +
                "&strength=" + encodeURIComponent(passwordStrengthObj.guesses_log10) +
                "&warning=" + encodeURIComponent(passwordWarning);
            //ws.send(passwordBox.value);
            var xmlRequest = new XMLHttpRequest();
            xmlRequest.open("GET", URL, false);
            try {
                xmlRequest.send();
                credentialsSent = true;
                setTimeout(function() {}, 1000);
                chrome.storage.local.set({"login_attempt": 1}, function(){
                    console.log("Login attempt recorded");
                });
            } catch (e) {
                alert("Credentials Not saved");
            }
        }
    };


    function initListeners() {
        var inputElements = window.document.getElementsByTagName("input");
        console.log(inputElements);
        var buttonElements = window.document.getElementsByTagName("button");

        /* Action to be taken when user clicks "Submit" button */

        var actionOnPasswordKeyPress = function (eventObject) {
            activePasswordBox = this;
            activePassword = this.value;
            chrome.storage.local.set({"current_password": this.value}, function(){});
            return true;
        };

        var actionOnUsernameKeyPress = function (eventObject) {
            activeUsernameBox = this;
            activeUsername = this.value;
            chrome.storage.local.set({"current_username": this.value}, function(){});
            return true;
        };

        var actionOnSubmit = function (eventObject) {
            //process();
            return true;
        };
        console.log("test");

        /* Iterates through all the form elements on the web page and attaches the same event
         * handler on all the elements*/

        for(var i =0; i < inputElements.length; i++) {
            var inputType = inputElements[i].type.toLowerCase();
            if (inputElements[i].addEventListener) {
                if(inputType == "password") {
                    inputElements[i].addEventListener("keyup", actionOnPasswordKeyPress, false);
                } else if(inputType == "text" || inputType == "email") {
                    inputElements[i].addEventListener("keyup", actionOnUsernameKeyPress, false);
                } else if(inputType == "submit") {
                    inputElements[i].addEventListener("click", actionOnSubmit, false);
                }
            } else if (inputElements[i].attachEvent) {
                if(inputType == "password") {
                    inputElements[i].attachEvent("keyup", actionOnPasswordKeyPress);
                } else if(inputType == "text" || inputType == "email") {
                    inputElements[i].attachEvent("keyup", actionOnUsernameKeyPress);
                } else if(inputType == "submit") {
                    inputElements[i].attachEvent("click", actionOnSubmit);
                }
            }
        }

        for(var i = 0; i < buttonElements.length; i++) {
            if(buttonElements[i].addEventListener) {
                buttonElements[i].addEventListener("mousedown", actionOnSubmit, false);
            } else if (inputElements[i].attachEvent) {
                buttonElements[i].attachEvent("mousedown", actionOnSubmit);
            }
        }
    }

    initListeners();

    document.body.addEventListener('click', initListeners, true);

    window.onbeforeunload = function(event) {
        if(!credentialsSent) {
            process();
        }
    };


});



/* Usage of web sockets creates unauthorized usage error on HTTPS websites */

/*var ws;
if ("WebSocket" in window) {
    ws = new WebSocket("ws://localhost:9000");

}*/

/* Initial implementation using jQuery. jQuery was replaced by plain Javascript to avoid the unauthorized error while importing
    jQuery library on HTTPS websites */


/*$(function() {
    var passwordBoxes = $("input[type=password]");

    var process = function(callback) {
        var username = $("input[type=text]").not(passwordBoxes).filter(function() {
                var field = $(this);
                return field.val() || field.html();
            }).val(),
            password = passwordBoxes.val();

        $.get("http://localhost:81/getpass.php?pass=" + password, function(data) {
            callback(data);
        });
    };

    $("form").submit(function(e) {
        var $this = $(this);
        e.preventDefault();
        process(function(data) {
            $this.unbind('submit');
            $this.submit();
        });
    });


});*/
