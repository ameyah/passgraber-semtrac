This is a Chrome extension to grab passwords from a webpage, semantically modify the same and send the transformed password to ISI server.

-This extension was developed as a part of USC Cloudsweeper project.